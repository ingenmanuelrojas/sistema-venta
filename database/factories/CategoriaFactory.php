<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Categoria;

$factory->define(Categoria::class, function (Faker $faker) {
    return [
        'nombre'      => $faker->word,
        'descripcion' => $faker->paragraph(1),
        'condicion'   => $faker->numberBetween(0,1),
    ];
});
