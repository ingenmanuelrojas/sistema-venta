<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
    protected $fillable =[
        'idcategoria','codigo','nombre','precio_venta','stock','descripcion','condicion'
    ];

    public function categoria(){
        //1 articulo pertenece a 1 categoria
        return $this->belongsTo('App\Categoria');
    }
}
